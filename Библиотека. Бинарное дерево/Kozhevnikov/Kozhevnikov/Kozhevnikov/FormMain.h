#pragma once
#include <Windows.h> 
#include <string>
#include <cmath>
#include <iostream>
#include <list>
#include <vector>
#include "BookTree.h"
#include "BookTr.h"
#include "AddForm.h"
#include "DeleteForm.h"
#include "SpisForm.h"
#include "SearchForm.h"
#include "FileHeader.h"

namespace Kozhevnikov {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Drawing::Drawing2D;
	using namespace System::Drawing::Imaging;

	std::string pathFileName;
	struct bookBTree* root = NULL;


	/// <summary>
	/// ������ ��� FormMain
	/// </summary>
	public ref class FormMain : public System::Windows::Forms::Form
	{

	
	public:
		FormMain(void)
		{
			struct bookBTree* root = NULL;
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~FormMain()
		{
			if (components)
			{
				delete components;
			}
		}


	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::PictureBox^ pictureBox1;
	private: System::Windows::Forms::ToolTip^ toolTip1;

	private: System::Windows::Forms::MenuStrip^ menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^ ����������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ���������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ��������������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ������������ToolStripMenuItem;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::ToolStripMenuItem^ �����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ����ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ ���������ToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^ �������ToolStripMenuItem;
	private: System::Windows::Forms::Button^ button1;
	private: System::ComponentModel::IContainer^ components;
	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->���������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->��������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->AutoScroll = true;
			this->panel1->BackColor = System::Drawing::SystemColors::ControlDark;
			this->panel1->Location = System::Drawing::Point(28, 69);
			this->panel1->Margin = System::Windows::Forms::Padding(0, 3, 0, 3);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(1163, 549);
			this->panel1->TabIndex = 11;
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::ControlDark;
			this->pictureBox1->Location = System::Drawing::Point(3, 3);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(100, 20);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox1->TabIndex = 12;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &FormMain::pictureBox1_Paint);
			// 
			// toolTip1
			// 
			this->toolTip1->ToolTipTitle = L"�����";
			// 
			// menuStrip1
			// 
			this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->����ToolStripMenuItem,
					this->����������ToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1200, 28);
			this->menuStrip1->TabIndex = 13;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// ����ToolStripMenuItem
			// 
			this->����ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->���������ToolStripMenuItem,
					this->�������ToolStripMenuItem
			});
			this->����ToolStripMenuItem->Name = L"����ToolStripMenuItem";
			this->����ToolStripMenuItem->Size = System::Drawing::Size(59, 24);
			this->����ToolStripMenuItem->Text = L"����";
			// 
			// ���������ToolStripMenuItem
			// 
			this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
			this->���������ToolStripMenuItem->Size = System::Drawing::Size(166, 26);
			this->���������ToolStripMenuItem->Text = L"���������";
			this->���������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::���������ToolStripMenuItem_Click);
			// 
			// �������ToolStripMenuItem
			// 
			this->�������ToolStripMenuItem->Name = L"�������ToolStripMenuItem";
			this->�������ToolStripMenuItem->Size = System::Drawing::Size(166, 26);
			this->�������ToolStripMenuItem->Text = L"�������";
			this->�������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::�������ToolStripMenuItem_Click);
			// 
			// ����������ToolStripMenuItem
			// 
			this->����������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {
				this->���������������ToolStripMenuItem,
					this->��������������ToolStripMenuItem, this->������������ToolStripMenuItem, this->�����ToolStripMenuItem
			});
			this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
			this->����������ToolStripMenuItem->Size = System::Drawing::Size(105, 24);
			this->����������ToolStripMenuItem->Text = L"����������";
			// 
			// ���������������ToolStripMenuItem
			// 
			this->���������������ToolStripMenuItem->Name = L"���������������ToolStripMenuItem";
			this->���������������ToolStripMenuItem->Size = System::Drawing::Size(201, 26);
			this->���������������ToolStripMenuItem->Text = L"�������� �����";
			this->���������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::���������������ToolStripMenuItem_Click);
			// 
			// ��������������ToolStripMenuItem
			// 
			this->��������������ToolStripMenuItem->Name = L"��������������ToolStripMenuItem";
			this->��������������ToolStripMenuItem->Size = System::Drawing::Size(201, 26);
			this->��������������ToolStripMenuItem->Text = L"������� �����";
			this->��������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::��������������ToolStripMenuItem_Click);
			// 
			// ������������ToolStripMenuItem
			// 
			this->������������ToolStripMenuItem->Name = L"������������ToolStripMenuItem";
			this->������������ToolStripMenuItem->Size = System::Drawing::Size(201, 26);
			this->������������ToolStripMenuItem->Text = L"������� �����";
			this->������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::������������ToolStripMenuItem_Click);
			// 
			// �����ToolStripMenuItem
			// 
			this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
			this->�����ToolStripMenuItem->Size = System::Drawing::Size(201, 26);
			this->�����ToolStripMenuItem->Text = L"�����";
			this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormMain::�����ToolStripMenuItem_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(1102, 31);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(86, 32);
			this->button3->TabIndex = 14;
			this->button3->Text = L"Refresh";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &FormMain::button3_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(1010, 31);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(86, 32);
			this->button1->TabIndex = 15;
			this->button1->Text = L"Clear";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &FormMain::button1_Click_1);
			// 
			// FormMain
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(10, 20);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1200, 729);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->menuStrip1);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MainMenuStrip = this->menuStrip1;
			this->Margin = System::Windows::Forms::Padding(0, 4, 0, 4);
			this->MaximizeBox = false;
			this->Name = L"FormMain";
			this->Text = L"FormMain";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion	



		/*
		/--------------------------------------------------------------------------------------------------------------------------/
		/--------------------------------------------------------------------------------------------------------------------------/
		/--------------------------------------------------------------------------------------------------------------------------/
		*/

		

		cli::array<Button^>^ b = gcnew cli::array<Button^>(100);
		
		void treeprint(struct bookBTree* p) {
			if (p != NULL) {
				treeprint(p->left);
				MessageBox::Show(System::Convert::ToString(p->key));

				treeprint(p->right);
			}
		}

		void drawTree(struct bookBTree* p,int x, float xOffset, int y, Graphics^ g, int & cnt, char d)
		{
			if (p != NULL) {
				System::Drawing::Pen^ p1 = gcnew System::Drawing::Pen(Color::Black, 2);
				System::Drawing::Brush^ b1 = gcnew System::Drawing::SolidBrush(Color::White);
				System::Drawing::Rectangle rect = System::Drawing::Rectangle(x, y, 50, 50);
				System::Drawing::Font^ f1 = gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(2));
				//System::Drawing::Rectangle rect = System::Drawing::Rectangle(x, );
				System::Drawing::Point po1 = System::Drawing::Point(x, y);
				System::Drawing::Point po2 = System::Drawing::Point(x - xOffset, y - 100);
				if (d == 'l' && y > 0) {
					po1 = System::Drawing::Point(x+25, y+25);
					po2 = System::Drawing::Point(x + xOffset*2+ 25, y - 100 + 25);
					g->DrawLine(p1, po1, po2);
					g->FillEllipse(b1, x + int(xOffset * 2), y - 100, 50, 50);
				}
				if (d == 'r' && y > 0) {
					po1 = System::Drawing::Point(x+25, y+25);					
					po2 = System::Drawing::Point(x - xOffset*2 + 25, y - 100 + 25);
					g->DrawLine(p1, po1, po2);
					g->FillEllipse(b1, x-int(xOffset*2), y-100, 50, 50);
				}

				
				
				g->DrawEllipse(p1, rect);
				g->FillEllipse(b1,rect);
				b1 = gcnew System::Drawing::SolidBrush(Color::Black);
				//g->DrawString(System::Convert::ToString(p->key), f1, b1, x+10, y+51);

				drawTree(p->right, x+xOffset, xOffset/2, y+100,g, cnt, 'r');

				drawTree(p->left, x - xOffset, xOffset / 2, y + 100,g, cnt, 'l');
			}
		}

		void addToolTips(struct bookBTree* p, int x, float xOffset, int y, Graphics^ g, int& cnt)
		{
			if (p != NULL) {
				
				b[cnt] = gcnew System::Windows::Forms::Button();
				b[cnt]->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 6.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					static_cast<System::Byte>(204)));
				b[cnt]->Location = System::Drawing::Point(x+12, y+12);
				b[cnt]->Name = L"button" + System::Convert::ToString(cnt);
				b[cnt]->Size = System::Drawing::Size(34, 34);
				b[cnt]->TabIndex = 12;
				b[cnt]->Text = System::Convert::ToString(p->b->udc);
				b[cnt]->BackColor = System::Drawing::Color::White;
				b[cnt]->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
				b[cnt]->FlatAppearance->BorderColor = System::Drawing::Color::White;
				//b[cnt]->UseVisualStyleBackColor = true;
				b[cnt]->Parent = panel1;
				toolTip1->SetToolTip(b[cnt], p->b->GetStr1());
				panel1->Controls->Add(b[cnt]);

				//g->DrawLine(p1, p2);
				cnt = cnt + 1;
				addToolTips(p->right, x + xOffset, xOffset / 2, y + 100, g, cnt);

				//MessageBox::Show(System::Convert::ToString(p->key));
				//System::String ^ str =  gcnew String(p->b->bookName.c_str());
				//MessageBox::Show(str);
				cnt = cnt + 1;
				addToolTips(p->left, x - xOffset, xOffset / 2, y + 100, g, cnt);
			}
		}

		void refreshDrawing()
		{
			panel1->Controls->Clear();
			Bitmap^ bmp = gcnew Bitmap(1100, 1000, PixelFormat::Format32bppArgb);
			Graphics^ g = Graphics::FromImage(bmp);
			//pictureBox1->
			double x = 500, y = 0;
			//root = deleteNode(root, 204);
			int cnt = 0;
			drawTree(root, x, x / 2, y, g, cnt, 'q');
			pictureBox1->Image = bmp;
			addToolTips(root, x, x / 2, y, g, cnt);
			panel1->Controls->Add(this->pictureBox1);
		}
		

		void test()
		{
			treeInit();
		}

		void treeInit()
		{
					
			//struct bookBTree* root = NULL;
			//root = NULL;
			Book b1 = Book(193, "�������� �.�.", "��� ��������� � ������", 2013, 32);
			root = addTree(root, b1, 0);
			
			b1 = Book(192, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(195, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(200, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(196, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(191, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);	

			b1 = Book(1, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(194, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(204, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(201, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(206, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(197, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);

			b1 = Book(202, "�������� �.�.", "��� ��������� � ������ ��", 2013, 1);
			root = addTree(root, b1, 0);
			
		
			//treeprint(root);
		}


		/*
		/--------------------------------------------------------------------------------------------------------------------------/
		/--------------------------------------------------------------------------------------------------------------------------/
		/--------------------------------------------------------------------------------------------------------------------------/
		*/



	

	private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) {

		Bitmap^ bmp = gcnew Bitmap(1100, 1000, PixelFormat::Format32bppArgb);
		Graphics^ g = Graphics::FromImage(bmp);
		double x = 500, y = 0;

		test();
		int cnt = 0;
		drawTree(root, x, x / 2, y, g, cnt, 'q');
		pictureBox1->Image = bmp;
		addToolTips(root, x, x / 2, y, g, cnt);
		panel1->Controls->Add(this->pictureBox1);

	}
	

	private: System::Void comboBox1_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void pictureBox1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) {
		
	}

private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {

	Book b1 = Book(8, "�������� �.�.", "��� ��������� � ������", 2013, 32);
	root = addTree(root, b1, 0);
	refreshDrawing();
}
private: System::Void ���������������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	//Form af = new Form AddForm();
	Form^ aForm = gcnew AddForm();
	aForm->ShowDialog();
	refreshDrawing();
	
}
private: System::Void ��������������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	Form^ aForm = gcnew SpisForm();
	aForm->ShowDialog();
	refreshDrawing();
}
private: System::Void ������������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	Form^ aForm = gcnew DeleteForm();
	aForm->ShowDialog();
	refreshDrawing();
}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	refreshDrawing();
}
private: System::Void �����ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	Form^ aForm = gcnew SearchForm();
	aForm->ShowDialog();
	refreshDrawing();
}
private: System::Void ���������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	SaveFileDialog^ sfd = gcnew SaveFileDialog;
	sfd->InitialDirectory = "C:\\Users\\�������\\Documents\\DefaultProjectDir";
	sfd->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
	if (sfd->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		pathFileName = cstr_to_usual(sfd->FileName);
		saveToFile(root, true);
	}
	
}
private: System::Void �������ToolStripMenuItem_Click(System::Object^ sender, System::EventArgs^ e) {
	//pathFileName = "test1.txt";
	OpenFileDialog^ ofd = gcnew OpenFileDialog;
	ofd->InitialDirectory = "C:\\Users\\�������\\Documents\\DefaultProjectDir";
	ofd->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
	if (ofd->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		pathFileName = cstr_to_usual(ofd->FileName);
		readFromFile();
		refreshDrawing();
	}
}
private: System::Void button1_Click_1(System::Object^ sender, System::EventArgs^ e) {
	root = NULL;
	refreshDrawing();
}
};



}
