#pragma once
#include <Windows.h> 
#include "BookTree.h""
#include <msclr\marshal_cppstd.h>

namespace Kozhevnikov
{
	//������ + �������� ������
	static int length(System::String^ str)
	{
		int a = str->Length;
		return a;
	}

	static bool numChecker(System::String^ str, int checkId)//checkId = 0 =>������������� �����, checkId = 1 => (��������) ������������� �����
	{
		if ((str[0] >= '0' && str[0] <= '9') || (str[0] == '-'))
		{
			for (int i = checkId; i < length(str); i++)
				if (str[i] < '0' || str[i] > '9')
					return false;
		}
		else return false;

		return true;
	}

	
	extern struct bookBTree* root;

	static struct bookBTree* addTree(struct bookBTree* p, Book w, int lev) {
		if (p == NULL) {

			p = (struct bookBTree*)malloc(sizeof(struct bookBTree));
			p->key = w.udc;
			p->b = new Book(w.udc, w.authorFio, w.bookName, w.publishYear, w.count);
			p->level = lev;
			p->left = p->right = NULL;
		}
		else if (w.udc < p->key)
			p->left = addTree(p->left, w, lev++);
		else if (w.udc > p->key)
			p->right = addTree(p->right, w, lev++);
		else
		{
			p->b->count = p->b->count+w.count;//���� id ���� ���������, �� �������� � ���-�� ���� � ����������			
		}
		return p;
	}

	static bool ifNodeIn(struct bookBTree* p, int k)
	{
		//bool rez = false;
		/*if (!p)
			return false;
		else*/ if (k == p->key)
			return true;
		else if (k < p->key && p->left != NULL)
			return ifNodeIn(p->left, k);
		else if (k > p->key && p->right != NULL)
			return ifNodeIn(p->right, k);
		else
			return false;
	}

	static void spisat(struct bookBTree* p, int k, int count)
	{
		//bool rez = false;
		if (k == p->key)
			p->b->count = p->b->count - count;//���� �� ���������� �� �����, �� �����
		else if (k < p->key && p->left != NULL)
			spisat(p->left, k, count);
		else if (k > p->key && p->right != NULL)
			spisat(p->right, k, count);

	}
	

	

	static struct bookBTree* deleteNode(struct bookBTree* root, int k)
	{
		// Base case
		if (root == NULL)
			return root;

		// Recursive calls for ancestors of
		// node to be deleted
		if (root->key > k) {
			root->left = deleteNode(root->left, k);
			return root;
		}
		else if (root->key < k) {
			root->right = deleteNode(root->right, k);
			return root;
		}

		// We reach here when root is the node
		// to be deleted.

		// If one of the children is empty
		if (root->left == NULL && root->right == NULL)
		{
			struct bookBTree* temp = root;
			//temp->b = new Book(root->left->b->udc, root->left->b->authorFio, root->left->b->bookName, root->left->b->publishYear, root->left->b->count);
			//temp->b = new Book(root->right->b->udc, root->right->b->authorFio, root->right->b->bookName, root->right->b->publishYear, root->right->b->count);
			//temp->b = root->right->b;
			delete root;
			return NULL;
		}
		else if (root->left == NULL) {
			struct bookBTree* temp = root->right;
			//temp->b = new Book(root->left->b->udc, root->left->b->authorFio, root->left->b->bookName, root->left->b->publishYear, root->left->b->count);
			//temp->b = new Book(root->right->b->udc, root->right->b->authorFio, root->right->b->bookName, root->right->b->publishYear, root->right->b->count);
			temp->b = root->right->b;
			delete root;
			return temp;
		}
		else if (root->right == NULL) {
			struct bookBTree* temp = root->left;
			//temp->b = root->left->b;
			//temp->b = new Book(root->left->b->udc, root->left->b->authorFio, root->left->b->bookName, root->left->b->publishYear, root->left->b->count);
			temp->b = root->left->b;
			delete root;
			return temp;
		}

		// If both children exist
		else {

			struct bookBTree* succParent = root;

			// Find successor
			struct bookBTree* succ = root->right;
			while (succ->left != NULL) {
				succParent = succ;
				succ = succ->left;
			}

			// Delete successor.  Since successor
			// is always left child of its parent
			// we can safely make successor's right
			// right child as left of its parent.
			// If there is no succ, then assign
			// succ->right to succParent->right
			if (succParent != root)
				succParent->left = succ->right;
			else
				succParent->right = succ->right;

			// Copy Successor Data to root
			root->key = succ->key;
			root->b = succ->b;

			// Delete Successor and return root
			delete succ;
			return root;
		}
	}

	static std::string cstr_to_usual(System::String^ managed)
	{
		std::string unmanaged = msclr::interop::marshal_as<std::string>(managed);
		return unmanaged;
	}
}