#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include "BookTr.h"
#include "BookTree.h"
namespace Kozhevnikov
{
	extern std::string pathFileName;

	static void writeStringToTree(std::string str)
	{
		System::String^ be = gcnew System::String(str.c_str());
		array<String^>^ strArr = be->Split('|');
		root = addTree(root, Book(System::Convert::ToInt32(strArr[0]), cstr_to_usual(strArr[1]), cstr_to_usual(strArr[2]), System::Convert::ToInt32(strArr[3]), System::Convert::ToInt32(strArr[4])),0);
	}

	static void saveToFile(struct bookBTree* p, bool first)
	{
		if (p != NULL)
		{
			if (first)
			{
				std::ofstream ofs;
				ofs.open(pathFileName, std::ofstream::out | std::ofstream::trunc);
				ofs.close();
				std::ofstream myfile(pathFileName, std::ios_base::app | std::ios_base::out);
				//myfile.open;
				myfile << p->b->GetStr() << "\n";
				myfile.close();
				saveToFile(p->right, false);
				saveToFile(p->left, false);
			}
			else
			{
				std::ofstream myfile(pathFileName, std::ios_base::app | std::ios_base::out);
				//myfile.open;
				myfile << p->b->GetStr() << "\n";
				myfile.close();
				saveToFile(p->right, false);
				saveToFile(p->left, false);

			}
		}
			
	}

	static void readFromFile()
	{
		root = NULL;
		std::string line;
		std::ifstream infile(pathFileName);
		root = NULL;
		while (std::getline(infile, line))
		{
			writeStringToTree(line);
		}
	}
}
